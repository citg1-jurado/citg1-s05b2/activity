<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register</title>
</head>
<body>
	<%
		String dateOfBirth = session.getAttribute("dateOfBirth").toString();
		dateOfBirth = dateOfBirth.replace("T", " / ");
	%>
	
	<h1>Registration Confirmation</h1>
	<p>First Name: <%= session.getAttribute("firstname") %></p>
	<p>Last Name: <%= session.getAttribute("lastname") %></p>
	<p>Phone: <%= session.getAttribute("phone") %></p>
	<p>Email: <%= session.getAttribute("email") %></p>
	<p>App Discovery: <%= session.getAttribute("appType") %></p>
	<p>Date of Birth: <%= dateOfBirth %></p>
	<p>User Type: <%= session.getAttribute("emap") %></p>
	<p>Description: <%= session.getAttribute("comments") %></p>
	
	<!-- Submit button for register -->
	<form action="login" method="post">
		<input type="submit">
	</form>
	
	<!-- Button to go back at index.jsp -->
	<form action="index.jsp">
		<input type="submit" value="Back">
	</form>	
	
</body>
</html>