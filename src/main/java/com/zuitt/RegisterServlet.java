package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3834552629113201265L;
	
	public void init() throws ServletException{
		System.out.println("RegisterServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		// Capture the user input from the register form
		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String appType = req.getParameter("app_type");
		String dateOfBirth = req.getParameter("date_of_birth");
		String emap = req.getParameter("emap");
		String comments = req.getParameter("comments");
		
		// Store all the data from the form into the session
		HttpSession session = req.getSession();
		
		session.setAttribute("firstname", firstname);
		session.setAttribute("lastname", lastname);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("appType", appType);
		session.setAttribute("dateOfBirth", dateOfBirth);
		session.setAttribute("emap", emap);
		session.setAttribute("comments", comments);
		
		res.sendRedirect("register.jsp");
		
	}
	
	public void destroy() {
		System.out.println("RegisterServlet has been finalized.");
	}
	
}
